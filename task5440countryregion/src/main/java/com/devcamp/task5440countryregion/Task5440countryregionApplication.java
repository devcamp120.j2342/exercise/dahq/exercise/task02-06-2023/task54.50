package com.devcamp.task5440countryregion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5440countryregionApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5440countryregionApplication.class, args);
	}

}
