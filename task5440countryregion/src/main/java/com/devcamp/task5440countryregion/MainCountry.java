package com.devcamp.task5440countryregion;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class MainCountry {
    @CrossOrigin
    @GetMapping("/countrys")
    public ArrayList<Country> listCountry() {
        Region region = new Region("HCM", "TP Hồ Chí Minh");
        Region region1 = new Region("HN", "TP Hà Nội");
        Region region2 = new Region("HP", "TP Hải Phòng");
        Region region3 = new Region("CT", "TP Cần Thơ");
        Region region4 = new Region("DN", "TP Đà Nẵng");

        ArrayList<Region> listRegions = new ArrayList<>();
        listRegions.add(region);
        listRegions.add(region1);
        listRegions.add(region2);
        listRegions.add(region3);
        listRegions.add(region4);

        Country country = new Country("USA", "USA", null);
        Country country1 = new Country("CND", "Canada", null);
        Country country2 = new Country("VN", "VietNam", listRegions);
        ArrayList<Country> countries = new ArrayList<>();
        countries.add(country1);
        countries.add(country2);
        countries.add(country);

        return countries;
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello";
    }

}
